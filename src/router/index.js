import Vue from 'vue'
import VueRouter from 'vue-router'

// import '../resources/js/custom.js'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/purple',
    name: 'Purple',
    component: function () {
      return import(/* webpackChunkName: "orange" */ '../views/Purple.vue')
    },
  },
  {
    path: '/orange',
    name: 'Orange',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "orange" */ '../views/Orange.vue')
    },
  },
  {
    path: '/green',
    name: 'Green',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "green" */ '../views/Green.vue')
    },
  },
]

const router = new VueRouter({
  routes
})

export default router
